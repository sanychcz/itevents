class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :link
      t.date :startdate
      t.date :enddate

      t.timestamps null: false
    end
  end
end
