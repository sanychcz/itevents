class ChangeDateFormatInMyTable < ActiveRecord::Migration
  def up
    change_column :events, :startdate, :datetime
    change_column :events, :enddate, :datetime
  end

  def down
    change_column :events, :startdate, :date
    change_column :events, :enddate, :date
  end
end
