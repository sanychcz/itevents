class ChangeColumnAddress < ActiveRecord::Migration
  def up
    rename_column :events, :place, :address
  end

  def down
    rename_column :events, :address, :place
  end
end
