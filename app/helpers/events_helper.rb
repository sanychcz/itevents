module EventsHelper

	def color_category(category)
		case category
		when 'Conference'
			content_tag(:span, :class => 'label') do
				category
			end
		when 'Seminar'
			content_tag(:span, :class => 'success label') do
				category
			end
		when 'Hackaton'
			content_tag(:span, :class => 'alert label') do
				category
			end
		when 'Meetup'
			content_tag(:span, :class => 'warning label') do
				category
			end
		else
			content_tag(:span, :class => 'secondary label') do
				category
			end
		end
	end
end