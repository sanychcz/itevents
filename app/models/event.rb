class Event < ActiveRecord::Base
	validates :title, presence: true
	validates :startdate, presence: true
	validates :enddate, presence: true

	CATEGORIES = ['Conference', 'Seminar', 'Hackaton', 'Meetup']

	scope :active, -> { where("enddate < ?", Time.now) }
	scope :finished, -> { where("enddate > ?", Time.now) }
end
